import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pocker_monster_app/model/MonsterItem.dart';

class ComponentMonsterItem extends StatefulWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;


  @override
  State<ComponentMonsterItem> createState() => _ComponentMonsterItemState();
}

class _ComponentMonsterItemState extends State<ComponentMonsterItem> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.monsterItem.hp;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
              '${widget.isLive ? widget.monsterItem.imgSrc : 'assets/eve.png'}',
              fit: BoxFit.fill,
            ),
          ),
          Text(widget.monsterItem.name),
          Text('총 HP ${widget.monsterItem.hp}'),
          Text('공격력 ${widget.monsterItem.speed}'),
          Text('방어력 ${widget.monsterItem.defCap}'),
          Text('스킬명 ${widget.monsterItem.skill}'),
          Text('타입 ${widget.monsterItem.monsterType}'),
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: LinearPercentIndicator(
              width: 200,
              lineHeight: 14,
              percent: _calculateHpPercent(),
              backgroundColor: Colors.grey,
              progressColor: Colors.blue,
              barRadius: Radius.circular(10),
            ),
          ),
          (widget.isMyTurn && widget.isLive) ?
              OutlinedButton(
                  onPressed: widget.callback,
                  child: Text('공격'),
              ) : Container(),
        ],
      ),
    );
  }
}
