import 'package:pocker_monster_app/model/MonsterItem.dart';

class StageInMonsters{
  MonsterItem monster1;
  MonsterItem monster2;

  StageInMonsters(this.monster1, this.monster2);

  factory StageInMonsters.fromJson(Map<String, dynamic> json) {
    return StageInMonsters(
        MonsterItem.fromJson(json['monster1']),
        MonsterItem.fromJson(json['monster2'])
    );
  }
}