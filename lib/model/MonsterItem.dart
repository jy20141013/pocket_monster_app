class MonsterItem {
  String imgSrc;
  String name;
  String evolution;
  String monsterType;
  num hp;
  num atkPower;
  num defCap;
  num speed;
  String skill;
  num stageId;
  num monsterId;

  MonsterItem(
      this.stageId,
      this.monsterId,
      this.evolution,
      this.imgSrc,
      this.name,
      this.monsterType,
      this.hp,
      this.atkPower,
      this.defCap,
      this.speed,
      this.skill
      );

  factory MonsterItem.fromJson(Map<String, dynamic> json) {
    return MonsterItem(
      json['monsterId'],
      json['stageId'],
      json['evolution'],
      json['imgsrc'],
      json['name'],
      json['monsterType'],
      json['hp'],
      json['atkPower'],
      json['defCap'],
      json['speed'],
      json['skill']
    );
  }
}