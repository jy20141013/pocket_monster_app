import 'package:pocker_monster_app/model/stage_in_monsters.dart';

class FightStageStateResult {
  StageInMonsters data;

  FightStageStateResult(this.data);

  factory FightStageStateResult.fromJson(Map<String, dynamic> json) {
    return FightStageStateResult(
        StageInMonsters.fromJson(json['data'])
    );

  }
}