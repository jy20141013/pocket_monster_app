import 'package:dio/dio.dart';
import 'package:pocker_monster_app/config/config_api.dart';
import 'package:pocker_monster_app/model/fight_stage_state_result.dart';

class RepoFightStage {
  Future<FightStageStateResult> getCurrentState() async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/current/state';

    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
    })
    );

    return FightStageStateResult.fromJson(response.data);
  }

  Future<FightStageStateResult> setStageIn(num monsterId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/new/monster-id/{monsterId}';

    final response = await dio.get(
      _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      ),
    );

    return FightStageStateResult.fromJson(response.data);
  }

  Future<FightStageStateResult> delStageMonster(num monsterId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/stage/out/stage-id/{stageId}';

    final response = await dio.get(
      _baseUrl,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return FightStageStateResult.fromJson(response.data);
  }
}