import 'package:flutter/material.dart';
import 'package:pocker_monster_app/model/MonsterItem.dart';
import 'package:pocker_monster_app/pages/page_test.dart';
import 'package:pocker_monster_app/repository/repo_fight_stage.dart';

class PageFightStage extends StatefulWidget {
  const PageFightStage({super.key});

  @override
  State<PageFightStage> createState() => _PageFightStageState();
}

class _PageFightStageState extends State<PageFightStage> {
  MonsterItem? monster1;
  MonsterItem? monster2;

  Future<void> _loadDetail() async {
    await RepoFightStage().getCurrentState()
        .then((res) => {
          setState(() {
            monster1 = res.data.monster1;
            monster2 = res.data.monster2;
          })
    });
  }

  Future<void> _delMonster(num stageId) async {
    await RepoFightStage().delStageMonster(stageId)
        .then((res) => {
          _loadDetail()
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('전투대기실'),
      ),
      body: _buildBody(context),
    );
  }


  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          monster1 != null ?
          Container(
            child: Column(
              children: [
                Text(monster1!.name),
                OutlinedButton(
                    onPressed: () {
                      _delMonster(monster1!.stageId);
                    },
                    child: Text('퇴장'),
                ),
              ],
            ),
          ) : Container(),
          monster2 != null ?
          Container(
            child: Column(
              children: [
                Text(monster2!.name),
                OutlinedButton(
                  onPressed: () {
                    _delMonster(monster2!.stageId);
                  },
                  child: Text('퇴장'),
                ),
              ],
            ),
          ) : Container(),

          monster1 != null && monster2 != null ? OutlinedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageTest(monster1: monster1!, monster2: monster2!)));
              },
              child: Text('배틀'),
          ) : Container(),
        ],
      ),
    );
  }
}
