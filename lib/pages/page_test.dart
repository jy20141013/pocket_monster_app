import 'dart:math';

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pocker_monster_app/components/component_monster_item.dart';
import 'package:pocker_monster_app/model/MonsterItem.dart';

class PageTest extends StatefulWidget {
  const PageTest({
    super.key,
    required this.monster1,
    required this.monster2
  });

  final MonsterItem monster1;
  final MonsterItem monster2;

  @override
  State<PageTest> createState() => _PageTestState();
}

class _PageTestState extends State<PageTest> {

  bool _isTurnMonster1 = true;

  bool _monster1Live = true;
  num _monster1CurrentHp = 0;

  bool _monster2Live = true;
  num _monster2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn();
    setState(() {
      _monster1CurrentHp = widget.monster1.hp;
      _monster2CurrentHp = widget.monster2.hp;
    });
  }

  void _calculateFirstTurn() {
    if (widget.monster1.speed < widget.monster2.speed) {
      setState(() {
        _isTurnMonster1 = false;
      });
    }
  }

  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    criticalArr.shuffle();

    bool isCritical = true;
    if (criticalArr[0] == 1) isCritical = true;

    num resultHit = myHitPower;
    if (isCritical) resultHit = resultHit * 2;
    resultHit = resultHit - targetDefPower;
    resultHit = resultHit.round();

    return resultHit;
  }

  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && (_monster1CurrentHp <=0)) {
      setState(() {
        _monster1Live = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp <= 0)) {
      setState(() {
        _monster2Live = false;
      });
    }
  }

  void _attMonster(num actionMonster) {
    num myHisPower = widget.monster1.atkPower;
    num targetDefPower = widget.monster2.defCap;

    if (actionMonster == 2) {
      myHisPower = widget.monster2.atkPower;
      targetDefPower = widget.monster1.defCap;
    }

    num resultHit = _calculateResultHitPoint(myHisPower, targetDefPower);

    if (actionMonster == 1) {
      setState(() {
        _monster2CurrentHp -= resultHit;
        if (_monster2CurrentHp <= 0) _monster2CurrentHp = 0;
        _checkIsDead(2);
      });
    } else {
      setState(() {
        _monster1CurrentHp -= resultHit;
        if(_monster1CurrentHp <= 0) _monster1CurrentHp = 0;
        _checkIsDead(1);
      });
    }

    setState(() {
      _isTurnMonster1 = !_isTurnMonster1;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('테스트'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        children: [
          ComponentMonsterItem(
              monsterItem: widget.monster1,
              callback: () {
                _attMonster(1);
              },
              isMyTurn: _isTurnMonster1,
              isLive: _monster1Live,
              currentHp: _monster1CurrentHp,
          ),
          ComponentMonsterItem(
            monsterItem: widget.monster2,
            callback: () {
              _attMonster(2);
            },
            isMyTurn: !_isTurnMonster1,
            isLive: _monster2Live,
            currentHp: _monster2CurrentHp,
          ),
        ],
      )
    );
  }
}
